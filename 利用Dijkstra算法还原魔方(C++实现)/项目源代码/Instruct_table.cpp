#include"SS.h"
Instruct_table::Instruct_table()
{
	next_ele=0;
	ins=new Instruct_List[MAX];
}

bool Instruct_table::add_ins(unsigned short *new_behave,int length)
{
	if(next_ele==MAXLISH)
		return 0;

	ins[next_ele].init_ins_List(length,new unsigned short[20]);
	int i1;
	for(i1=0;i1<=19;i1++)
		ins[next_ele].content[i1]=0;

	 i1=0;
	while((new_behave[i1]))
	{
		ins[next_ele].content[i1]=new_behave[i1];
		i1++;
	}
	next_ele++;
	return 1;
}



bool Instruct_table::Is_behaves_exist(unsigned short *new_behave,int level)
{
	unsigned char MF_0[6][8],MF_1[6][8],MF_2[6][8];
	int i1;
	this->sub_Is_behaves_exist(new_behave,MF_0,MF_1,MF_2);

	run run_tool("run1.txt");
	Score score_tool;
	PictAndReveal pict;

	behave_buffer buffer;
	static int pre_level=21;

	if(pre_level!=level)
	{	pre_level=level;
	buffer.load_instructs("binstruct.txt",level);}

	i1=0;
	while(buffer.buffer[i1]&&(i1<=19))
	{
		run_tool.runMF(MF_2,buffer.buffer[i1],ins);
		if(score_tool.Is_the_same_status(MF_1,MF_2,1048575))
			return 1;
		i1++;
		pict.pictMF(MF_0,MF_2);
	}


	return 0;
}

int Instruct_table::Is_behaves_exist(unsigned short *new_behave,unsigned short pre_behaves[20])
{
	unsigned char MF_0[6][8],MF_1[6][8],MF_2[6][8];
    sub_Is_behaves_exist(new_behave,MF_0,MF_1,MF_2);
	PictAndReveal pict;
	run run_tool("run1.txt");
	Score score_tool;int i1=0;

	while(pre_behaves[i1]&&(i1<=19))
	{
		run_tool.runMF(MF_2,pre_behaves[i1],ins);
		if(score_tool.Is_the_same_status(MF_1,MF_2,1048575))
			return pre_behaves[i1];
		pict.pictMF(MF_0,MF_2);
		i1++;
	}
	
return 0;
}

bool Instruct_table::Is_behaves_exist(unsigned short *new_behave)
{
	unsigned char MF_0[6][8],MF_1[6][8],MF_2[6][8];
    sub_Is_behaves_exist(new_behave,MF_0,MF_1,MF_2);
	PictAndReveal pict;
	run run_tool("run1.txt");
	Score score_tool;int i1;

	for(i1=0;i1<=next_ele-1;i1++)
	{
		run_tool.runMF(MF_2,i1,ins);
	if(score_tool.Is_the_same_status(MF_1,MF_2,1048575))
	       return 1;
		pict.pictMF(MF_0,MF_2);
	}
	
return 0;
}

void Instruct_table::sub_Is_behaves_exist(unsigned short *new_behave,
	unsigned char MF_0[6][8],unsigned char MF_1[6][8],unsigned char MF_2[6][8])
{
	int i1,i2;
	for(i1=0;i1<=5;i1++)
		for(i2=0;i2<=7;i2++)
			MF_0[i1][i2]=i1;

	PictAndReveal pict;
	pict.pictMF(MF_0,MF_1);
	pict.pictMF(MF_0,MF_2);

	run run_tool("run1.txt");
	i1=0;
	while(new_behave[i1])
	run_tool.runMF(MF_1,new_behave[i1++],ins);
}

bool Instruct_table::renew_behave(unsigned short *new_behave,int i1)
{
	int pre_length=ins[i1].length;
	ins[i1].length=comput_behave_length(new_behave);

	int i2;bool finish=0;
	for(i2=0;i2<=19;i2++)
	{
		if(new_behave[i2]&&(!finish))
		{
			ins[i1].content[i2]=new_behave[i2];
		}
		else
		{
			finish=1;
		ins[i1].content[i2]=0;
		}
	}

	int i3;
	for(i2=0;i2<=next_ele-1;i2++)
	{   
		int k=0;
		for(i3=0;i3<=19;i3++)
		{
			k+=(ins[i2].content[i3]==i1)?1:0;
		}
		ins[i2].length-=k*(pre_length-ins[i1].length);
	}

	return 1;
}

short Instruct_table::comput_behave_length(unsigned short *new_behave)
{
	int i1=0;
		short sum=0;
	while(new_behave[i1]&&(i1<=19))
		sum+=ins[new_behave[i1++]].length;
	return sum;
	
}

bool Instruct_table::Has_relation(unsigned short behave1,unsigned short behave2)
{
	if(behave2<=behave1)
		return 0;

	int i1;
	for(i1=0;i1<=19;i1++)
	{
		if(ins[behave2].content[i1]==behave1)
			return 1;
	}
	return 0;
}

bool Instruct_table::Has_relation(MFG &pwh,unsigned short behave1,int its_layer,bool k[MAXLISH],int lengthOfk)
{
	if(its_layer>=pwh.level)
		return 0;

	int i1;
	for(i1=its_layer+1;i1<=pwh.level;i1++)
	{
		Behave *p=pwh.Graphics[i1-1].first_status[23].behavehand.first_behave;
		while(p)
		{
			if(k[p->name])
			{
				if(Has_relation(behave1,p->name))
					return 1;
			}
			p=p->next_behave;
		}
	}

	return 0;
}

bool Instruct_table::ck_avail_behave(MFG &pwh,bool k[MAXLISH],int lengthOfk)
{
	int i1;
	for(i1=pwh.level;i1>=1;i1--)
	{
		Behave *p=pwh.Graphics[i1-1].first_status[23].behavehand.first_behave;
		while(p)
		{
			if(!k[p->name])
			{
				if(Has_relation(pwh,p->name,i1,k,lengthOfk))
					k[p->name]=1;
			}
			p=p->next_behave;
		}
	}
	return 1;
}

bool Instruct_table::merge(Instruct_table ins2,unsigned short pre_behaves[20])
{
	int i1;
	for(i1=1;i1<=ins2.next_ele-1;i1++)
	{
		int result=Is_behaves_exist(ins2.ins[i1].content,pre_behaves);
		if(!result)
		{
			this->add_ins(ins2.ins[i1].content,ins2.ins[i1].length);
		}
		else
		{
			if(ins2.ins[i1].length<=this->ins[result].length)
			{
				this->renew_behave(ins2.ins[i1].content,result);
			}
		}
	}
	return 1;
}



	
