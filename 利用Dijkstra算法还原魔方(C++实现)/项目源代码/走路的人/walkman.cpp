#include"../SS.h"
Walkman::Walkman(bool hasmemory1):hasmemory(hasmemory1)
{
	checker=NULL;
    if(hasmemory)
    {memory=new Status*[MAX];
	memory_num=0;
    }
}

Walkman::Walkman(bool hasmemory1,Status *pos):hasmemory(hasmemory1)
{
	checker=pos;
	 if(hasmemory)
    {memory=new Status*[MAX];
	memory_num=0;
    }
}

bool Walkman::change_pos(Status *pos)
{
	if(hasmemory)
		return 0;
	checker=pos;
}

Status* Walkman::watch_road(int which_way)
{
	int num=checker->behavehand.num;
	static int front_which_way;
	static Status *front_checker=NULL;
	static Behave *front_behaver=NULL;
	int needsteps;
	Behave *begin_behave=NULL;
	if((front_checker==checker)&&((which_way%num)>=(front_which_way%num)))
	{
		needsteps=which_way%num-front_which_way%num;
		begin_behave=front_behaver;
	}
	else
	{
		needsteps=which_way;
		begin_behave=checker->behavehand.first_behave;
	}

	while(needsteps--)
	{
		begin_behave=begin_behave->next_behave;
	}

	front_which_way=which_way;
	front_checker=checker;
	front_behaver=begin_behave;

	return begin_behave->next_status;
}

bool Walkman::move(int which_way,bool sign)
{
	if(!hasmemory)
	return 0;

	Status* p=watch_road(which_way);
	if((p->donotvisited==0)&&(p->hasvisited==0))
	{
		if(push(checker))
		{
		   checker=p;
           p->hasvisited|=sign;
		   return 1;
		}
		return 0;
	}
	return 0;
}

bool Walkman::push(Status *pos)
{
	if(memory_num==MAX)
		return 0;
	memory_num++;
	*(memory++)=pos;
	return 1;
}

bool Walkman::pop(Status *&pos)
{
	if(memory_num==0)
		return 0;
	memory_num--;
	pos=*(--memory);
	return 1;
}

bool Walkman::back()
{
    if(!hasmemory)
	return 0;

	Status *pos;
	if(pop(pos))
	{
       checker=pos;
	   return 1;
	}
	return 0;		
}
extern int llllllll;
bool Walkman::move(GraphicOfStatus &map,short mark[24],Instruct_List ins_list[MAXLISH],short &move_step)
{
	if(hasmemory)
		return 0;
	Behave *now_behave=checker->behavehand.first_behave;
	int min_step=30000;
	Behave* best_behave=NULL;
	
	while(now_behave)
	{
		int index=now_behave->next_status-map.first_status;
		if(min_step>(mark[index]+ins_list[now_behave->name].length))
		{
        best_behave=now_behave;
		min_step=mark[index]+ins_list[now_behave->name].length;
		}
		now_behave=now_behave->next_behave;
	}
		
	
	checker=best_behave->next_status;

	move_step=best_behave->name;

	return 1;
}

bool Walkman::sub_ck_usefulbehave(GraphicOfStatus &map,short mark[24],Instruct_List ins_list[MAXLISH],bool k[MAXLISH])
{
		if(hasmemory||checker->donotvisited)
		return 0;

		Score score_tool;
		if(score_tool.Is_perfect_status(checker->status))
        return 0;

	Behave *now_behave=checker->behavehand.first_behave;
	int min_step=30000;
	Behave* best_behave=NULL;

		while(now_behave)
	{
		int index=now_behave->next_status-map.first_status;
		if(min_step>(mark[index]+ins_list[now_behave->name].length))
		{
			int i1;
			for(i1=0;i1<=MAXLISH-1;i1++)
				k[i1]=0;
        best_behave=now_behave;
		min_step=mark[index]+ins_list[now_behave->name].length;
		k[now_behave->name]=1;
		}
		else if(min_step==(mark[index]+ins_list[now_behave->name].length))
			k[now_behave->name]=1;

		now_behave=now_behave->next_behave;
	}
		return 1;
}


