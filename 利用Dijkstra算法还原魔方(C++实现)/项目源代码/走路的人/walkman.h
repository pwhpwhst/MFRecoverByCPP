
#include<stdio.h>

#ifndef NULL
#define NULL 0
#endif

class Walkman
{private:
	static const int MAX=24;
public:
	Status *checker;
	const bool hasmemory;
	Status **memory;
	int memory_num;

public:
	Walkman(bool hasmemory1);
	Walkman(bool hasmemory1,Status *pos);
	Status* watch_road(int which_way);
	bool move(int which_way,bool sign);
	bool back();
    bool move(GraphicOfStatus &map,short mark[24],Instruct_List ins_list[MAXLISH],short &move_step);
	bool change_pos(Status *pos);
	bool sub_ck_usefulbehave(GraphicOfStatus &map,short mark[24],Instruct_List ins_list[MAXLISH],bool k[MAXLISH]);
private:
	bool push(Status *pos);
	bool pop(Status *&pos);
};



