#include"SS.h"
behave_buffer::behave_buffer()
{
	buffer=new unsigned short[MAX];
	int i1;
	for(i1=0;i1<=MAX-1;i1++)
	{
		buffer[i1]=0;
	}
	next_ele=0;
}

behave_buffer::~behave_buffer()
{
	delete(buffer);
}

void behave_buffer::load_instructs(char* filename,int level)
{
	FILE* fp=fopen(filename,"rb");
	fseek(fp,40*(level-1),0);
	int i1;
	for(i1=0;i1<=19;i1++)
	{
		unsigned short tl,th;
		tl=fgetc(fp);th=fgetc(fp);
		buffer[i1]=tl+(th<<8);
		if(!buffer[i1])
		{this->next_ele=i1;
		break;}
	}
	fclose(fp);
}

void behave_buffer::add(unsigned short behave)
{
	buffer[next_ele]=behave;
	next_ele++;
}