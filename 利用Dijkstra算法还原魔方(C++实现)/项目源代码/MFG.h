
class MFG
{
public:
	GraphicOfStatus Graphics[20];
	unsigned char recover_order[20];
	int level;
public:
	MFG(unsigned char recover_order1[20]);
	MFG(unsigned char recover_order1[20],int level1);
	void Init_bottom_net(Instruct_table &ins);
	void renew_nets(unsigned char behavesFornets[20][20],Instruct_List ins[MAXLISH]);
	void renew_mark(short pmark[20][24],short mark[20][24],Instruct_List ins[MAXLISH]);
	void recover(unsigned char MF[6][8],short mark[20][24],Instruct_List ins[MAXLISH],short solve[20][20]);
	bool ck_useful_behave(short mark[20][24],Instruct_table &ins,bool k[MAXLISH]);
	void creat_higher_net(short mark[20][24],short pmark[20][24],Instruct_table &ins,Data_manage &h);

private:
	void pre_creat_higher_net(short mark[20][24],Instruct_table &ins);
	bool acquire_new_behave(short mark[20][24],Instruct_List ins[MAXLISH],unsigned short new_solve[20]);
	bool try_creat_behaves(short mark[20][24],int behave_length,short *rans,Instruct_List ins[MAXLISH],unsigned short new_solve[20]);
	bool Init_higher(unsigned char layer);
	void get_random(int num,short rans[10]);
    void attach_behaves(short rans[10],short solve[20][20],unsigned short new_solve[20],int deep);
	bool Is_next_final_layer();
	bool pre_creat_higher_net(Instruct_table &ins);
};
