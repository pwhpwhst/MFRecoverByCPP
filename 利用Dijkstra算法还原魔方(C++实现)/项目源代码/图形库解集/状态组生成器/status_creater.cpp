#include"../../SS.h"

status_creater::status_creater()
{
	which_grid=0;
	int i1,i2;
    for(i1=0;i1<=23;i1++)
		for(i2=0;i2<=5;i2++)
		{
			status[i1][i2]=0;
		}
}

void  status_creater::status_2(unsigned char col[2])
{
	Clear_status();
	int i1,i2,t=0;
	for(i1=0;i1<=11;i1++)
	{
		for(i2=0;i2<=2;i2++)
		{
			status[2*i1][2*i2]=in_2[i1][i2];
			status[2*i1+1][2*i2]=in_2[i1][i2];

			if(in_2[i1][i2]!=6)
			{
				status[2*i1][2*i2+1]=col[(t++)%2];
				status[2*i1+1][2*i2+1]=col[t%2];
			}
			else
			{
				status[2*i1][2*i2+1]=6;
				status[2*i1+1][2*i2+1]=6;
			}

		}
	}
	which_grid=2;
}

void  status_creater::status_3(unsigned char col[3])
{int i1,i2,i3,sum1=col[0]+col[1]+col[2];
unsigned char anti_ord_col[3]={col[0],col[2],col[1]};
unsigned char *p;

Clear_status();
	for(i1=0;i1<=7;i1++)
	{
		if((in_3[i1][0]+in_3[i1][1]+in_3[i1][2]+sum1)%2)
		p=anti_ord_col;
		else
		p=col;

		for(i2=0;i2<=2;i2++)
			for(i3=0;i3<=2;i3++)
				{status[3*i1+i2][2*i3]=in_3[i1][i3];
		         status[3*i1+i2][2*i3+1]=p[(i3+i2)%3];
		        }
		
	}
	which_grid=3;
}

void status_creater::status_(unsigned char col[3])
{
	unsigned char i1,t=3;
	for(i1=0;i1<=2;i1++)
	{
		if(col[i1]==6)
		{
			if(i1!=2)
			{
				col[i1]=col[2];
			}
			status_2(col);
			return ;
		}
	}
	status_3(col);
}

void status_creater::Clear_status()
{
	which_grid=0;
	int i1,i2;
    for(i1=0;i1<=23;i1++)
		for(i2=0;i2<=5;i2++)
		{
			status[i1][i2]=0;
		}
}

const char status_creater::in_2[12][3]={{0,6,4},{0,2,6},{0,6,5},{0,3,6},{6,2,4},{6,2,5},{6,3,5},
{6,3,4},{1,6,4},{1,2,6},{1,6,5},{1,3,6}};

const char status_creater::in_3[8][3]={{0,2,4},{0,2,5},{0,3,5},{0,3,4},{1,2,4},{1,2,5},{1,3,5},{1,3,4}};

