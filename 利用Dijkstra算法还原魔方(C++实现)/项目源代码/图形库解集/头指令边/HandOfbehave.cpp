#include"../../SS.h"


HandOfbehave::HandOfbehave()
{
	num=0;
    first_behave=NULL;
}

HandOfbehave::HandOfbehave(int num1,class Behave *first_behave1)
{
	num=num1;
    first_behave=first_behave1;
}

void HandOfbehave::add_behave(Behave* newbehave)
{
    int i1=num;
	Behave *p=first_behave;

	if(!i1)
	{
       first_behave=newbehave;
	   num=1;
	}		
	else
	{
         while(--i1)
	   {
		p=p->next_behave;
	   }
	    p->next_behave=newbehave;
	    num++;
	}
}

void HandOfbehave::clean_HandOfbehave()
{
	Behave *p1=first_behave,*p2;
	while(p1)
	{
		p2=p1;
		p1=p1->next_behave;
		p2->~Behave();
	}
	first_behave=NULL;
	num=0;
}

HandOfbehave::~HandOfbehave()
{
	clean_HandOfbehave();
}

