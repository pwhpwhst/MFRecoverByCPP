#include"../../SS.h"

void reveal_status(unsigned char status[6]);

GraphicOfStatus::GraphicOfStatus(){};

GraphicOfStatus::GraphicOfStatus(int grid1)
{
	Init_GraphicOfStatus(0,grid1);
}

GraphicOfStatus::GraphicOfStatus(unsigned int grid_need_compare1,int grid1)
{
	Init_GraphicOfStatus(grid_need_compare1,grid1);
}

void GraphicOfStatus::Init_GraphicOfStatus(unsigned int grid_need_compare1,int grid1)
{
	Score score_tool;
	status_creater status_tool;
	unsigned char fac[3]={0};

	grid_need_compare=grid_need_compare1;
	grid=grid1;
	
	score_tool.get_fac(grid,fac);

	status_tool.status_(fac);

	first_status=new Status[24];
	int i1,t;
	for(i1=0;i1<=23;i1++)
	{
		first_status[i1].init(status_tool.status[i1]);
		
		t=score_tool.get_grid_from_status_fac(status_tool.status[i1]);
		if(((grid_need_compare1>>(t-1))%2))
		{
			first_status[i1].donotvisited=1;
		}
	}
}

void GraphicOfStatus::clean_sign()
{
	int i1;
	for(i1=0;i1<=23;i1++)
		first_status[i1].hasvisited=0;	
}

void GraphicOfStatus::Traversing()
{
	clean_sign();
	int i1=0;
	while(first_status[i1].donotvisited)
		i1++;

	Walkman pwh(1,first_status+i1);
	pwh.checker->hasvisited=1;
	
	int num=pwh.checker->behavehand.num;
     i1=0;
	while(1)
	{
    for(i1=0;i1<=num-1;i1++)
	{
			if(pwh.move(i1,1))
			{
               break;
			}
				
				
	}
	if(i1==num)
		if(!pwh.back())
			break;
	
	}
	
}

void GraphicOfStatus::Traversing(int t)
{
	clean_sign();
	int i1=0;

	Walkman pwh(1,first_status+t);
	reveal_status(pwh.checker->status);
	pwh.checker->hasvisited=1;
	
	int num=pwh.checker->behavehand.num;
     i1=0;
	while(1)
	{
    for(i1=0;i1<=num-1;i1++)
	{
			if(pwh.move(i1,1))
			{
				reveal_status(pwh.checker->status);
               break;
			}
		
	}
	if(i1==num)
		if(!pwh.back())
			break;
		else
		{
			reveal_status(pwh.checker->status);
		}
	}
	
}

void GraphicOfStatus::renew_net_mark(Status *destinnation,short pmark[24],short mark[24],Instruct_List ins_list[MAXLISH])
{
	unsigned int hasvisit=0,i1=0,u=1,i2;
	int k=0;
	for(i1=0,i2=0;i1<=23;i1++)
	{
		if(first_status[i1].donotvisited==1)
		{
           hasvisit+=u<<i1;
		   int t=i2,tt=pmark[t];
		   for(;i2<=22;i2++)
			   pmark[i2]=pmark[i2+1];
		   pmark[23]=tt;
		   i2=t;
		}
		else i2++;
	}

	if(find_index(destinnation->status,k))
	{
	mark[k]=0;
	pmark[0]=k;
	}

    i1=0;
	while(hasvisit<16777215)//2^24-1=16777215
	{
		
		if(Is_renewable_status(pmark[i1],hasvisit))
		{			
        hasvisit|=u<<pmark[i1];
		renew_status_mark(pmark,mark,pmark[i1],ins_list);
		i1=0;
		}
		else
		{
         i1++;
		}		
	}
}

void GraphicOfStatus::renew_net_mark(short pmark[24],short mark[24],Instruct_List ins_list[MAXLISH])
{
	Score score_tool;
	unsigned char fac[3]={0};
	score_tool.get_fac(grid,fac);
	unsigned char status[6]={0};
	int i1;
	for(i1=0;i1<=2;i1++)
		status[2*i1]=status[2*i1+1]=fac[i1];
	Status *destinnation=find_status(status);

	renew_net_mark(destinnation,pmark,mark,ins_list);
}

bool GraphicOfStatus::Is_renewable_status(int i1,unsigned int hasvisit)
{
	if((hasvisit<<(31-i1))>>31==1)
		return 0;
	return 1;
}

void GraphicOfStatus::renew_status_mark(short pmark[24],short mark[24],int index,Instruct_List ins_list[MAXLISH])
{
	void check_status_mark(GraphicOfStatus &a,short mark[24]);
	int i1=0,t=0;
	Behave *now_behave=first_status[index].behavehand.first_behave;

	while(now_behave)
	{
		if(now_behave->front_status->donotvisited);
		else
		{
			int sub_index;
			find_index(now_behave->front_status->status,sub_index);
		short pre_mark=mark[sub_index];
		short now_mark=mark[index]+ins_list[now_behave->name].length;
		if(now_mark<pre_mark)
		{
            mark[sub_index]=now_mark;
           sort(pmark,sub_index,mark);
		}
			
		}
		now_behave=now_behave->next_behave;
	}
	
}

class Status * GraphicOfStatus::find_status(unsigned char status[6])
{
	int i1;
	Score score_tool;
	for(i1=0;i1<=23;i1++)
	{
		if(score_tool.Is_the_same_status(status,first_status[i1].status))
			return first_status+i1;
	}
	return NULL;
}

bool GraphicOfStatus::find_index(unsigned char status[6],int &index)
{
	class Status *p=find_status(status);
	if(!p) return 0;
	index=p-first_status;
	return 1;
}

void GraphicOfStatus::renew_Data_net(unsigned short behave1,Instruct_List inc_list[MAXLISH])
{
	int i1;
	Behave_effect behaveeff;
	unsigned char status[6]={0};
	for(i1=0;i1<=23;i1++)
	{
		if(first_status[i1].donotvisited==0)
		{
			behaveeff.check_effect_to_status(1,behave1,inc_list,first_status[i1].status,status);
            class Status* now_status1=find_status(status);
			behaveeff.check_effect_to_status(0,behave1,inc_list,first_status[i1].status,status);
            class Status* now_status2=find_status(status);
			
			if(now_status1&&now_status2)
			{
                first_status[i1].LinkOther_status(now_status1,now_status2,behave1);
			}
			
		}
	}
}

unsigned int GraphicOfStatus::Is_Connected_Graphic()
{
	Traversing();
	int i1;
	unsigned int t=0,u=1;
	for(i1=1;i1<=24;i1++)
	{
		bool hasvisited=first_status[i1-1].hasvisited;
		bool donotvisited=first_status[i1-1].donotvisited;
		if(hasvisited||donotvisited)
		{
			t+=u<<(i1-1);
		}
	}

	return t;	
}

unsigned char GraphicOfStatus::find_pnum(short pmark[24],short num)
{unsigned char i1=0;
     while(pmark[i1++]!=num){}
	 return --i1;
}

unsigned char GraphicOfStatus::find_num(short pmark[24],unsigned char pmark_index,short mark[24])
{
	short num=mark[pmark[pmark_index]];
	
	if(num>=mark[pmark[23]])
		return 23;
	if(num<=mark[pmark[0]])
		return 0;
	int min=0,max=23,result;

	switch(pmark_index)
	{
	case 0:mark[pmark[0]]=mark[pmark[1]];break;
	case 23:mark[pmark[23]]=mark[pmark[22]];break;
	default:short t1=mark[pmark[pmark_index-1]];
	short t2=mark[pmark[pmark_index+1]];
	mark[pmark[pmark_index]]=((t1+t2)-(t1+t2)%2)/2;
	}


	while(max-min>1)
	{
		int u=((max+min)-(max+min)%2)/2;
		if(mark[pmark[u]]==num) 
		{result=u;break;}
		else if(num<mark[pmark[u]])
			max=u;
		else min=u;
	}

	if(max-min==1)
	{
	if(pmark_index>=max)
		result=max;
	if(pmark_index<=min)
		result=min;
	}
	mark[pmark[pmark_index]]=num;
	return result;
}

void GraphicOfStatus::exchange(short pmark[24],int index,int muti_index)
{
	int i1,t=pmark[index];
	if(index<muti_index)
	{
		for(i1=index+1;i1<=muti_index;i1++)
			pmark[i1-1]=pmark[i1];
	}
	else
	{
		for(i1=index-1;i1>=muti_index;i1--)
			pmark[i1+1]=pmark[i1];
	}
	pmark[muti_index]=t;
}

void GraphicOfStatus::sort(short pmark[24],unsigned char status_index,short mark[24])
{
	unsigned char pmark_index1=find_pnum(pmark,status_index);
	unsigned char pmark_index2=find_num(pmark,pmark_index1,mark);
	exchange(pmark,pmark_index1,pmark_index2);
}

bool GraphicOfStatus::ck_useful_behave(short mark[24],Instruct_table &ins,bool k[MAXLISH])
{
	int i1;
	bool buffer_k[MAXLISH];
	Walkman A(0);

	for(i1=0;i1<=23;i1++)
	{
		int i2=0;
		for(i2=0;i2<=MAXLISH-1;i2++)
			buffer_k[i2]=0;

		A.change_pos(this->first_status+i1);
		if(A.sub_ck_usefulbehave(*this,mark,ins.ins,buffer_k))
		{
			for(i2=0;i2<=MAXLISH-1;i2++)
			k[i2]|=buffer_k[i2];
		}
	}
	return 1;
}

void GraphicOfStatus::clean()
{
	int i1;
	for(i1=0;i1<=23;i1++)
	{
        first_status[i1].behavehand.clean_HandOfbehave();
	}
}

bool GraphicOfStatus::sub_solve(unsigned char status[6],short solve_course[20],short mark[24],Instruct_List ins_list[MAXLISH])
{
	int index;
	if(!find_index(status,index))
		return 0;

    int i1;
	for(i1=0;i1<=19;i1++)
		solve_course[i1]=0;
	
	Walkman walkman(0,first_status+index);
	
	short next_step;

	Score score_tool;
	for(i1=0;i1<=23;i1++)
		if(score_tool.Is_perfect_status(first_status[i1].status))
			break; 
	int i2=0;
	while(walkman.checker-(first_status+i1))
	{
	walkman.move(*this,mark,ins_list,next_step);
	solve_course[i2++]=next_step;

	}	

		
	return 1;
}