
class GraphicOfStatus
{
	friend class MFG;
friend void main();
	public:
	int grid;
	unsigned int grid_need_compare;
	class Status *first_status; 
public:
	GraphicOfStatus();
	GraphicOfStatus(int grid1);
	GraphicOfStatus(unsigned int grid_need_compare1,int grid1);
	void Init_GraphicOfStatus(unsigned int grid_need_compare1,int grid1);
	void renew_Data_net(unsigned short behave1,Instruct_List inc_list[MAXLISH]);
	unsigned int Is_Connected_Graphic();
	void renew_net_mark(Status *destinnation,short pmark[24],short mark[24],Instruct_List ins_list[MAXLISH]);	
	void renew_net_mark(short pmark[24],short mark[24],Instruct_List ins_list[MAXLISH]);
	bool sub_solve(unsigned char status[6],short solve_course[20],short mark[24],Instruct_List ins_list[MAXLISH]);
    void GraphicOfStatus::renew_Data_net(bool other_list,unsigned short behave1,Instruct_List inc_list[MAXLISH]);
	void clean();
private:
	void renew_status_mark(short pmark[24],short mark[24],int index,Instruct_List ins_list[MAXLISH]);
    void clean_sign();
	void Traversing();
	void Traversing(int t);
	class Status * find_status(unsigned char status[6]);
    unsigned char find_num(short pmark[24],unsigned char pmark_index,short mark[24]);
	unsigned char find_pnum(short pmark[24],short num);
    void exchange(short pmark[24],int index,int muti_index);
	bool find_index(unsigned char status[6],int &index);
	void sort(short pmark[24],unsigned char status_index,short mark[24]);
	bool Is_renewable_status(int i1,unsigned int hasvisit);
	bool ck_useful_behave(short mark[24],Instruct_table &ins,bool k[MAXLISH]);
};

