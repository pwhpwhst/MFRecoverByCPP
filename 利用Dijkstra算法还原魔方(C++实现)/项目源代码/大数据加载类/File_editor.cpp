#include"../SS.h"

File_editor::File_editor(int num):Data_buffer(num){}

void File_editor::IntoBuffer(int grid)
{
    unsigned char t0,t1;
	char filename[]="Grid_00situation.txt";
	t0=grid%10;
	t1=(grid-grid%10)/10;
	filename[5]=t1+48;
	filename[6]=t0+48;


	FILE* fp;
	fp=fopen(filename,"rb");

	int i1=0;
	while(!feof(fp))
		buffer[i1++]=fgetc(fp);
	buffer_length=i1-1;
	fclose(fp);
}

void File_editor::OutBuffer(int grid)
{
	unsigned char t0,t1;
	char filename[]="Grid_00situation.txt";
	t0=grid%10;
	t1=(grid-grid%10)/10;
	filename[5]=t1+48;
	filename[6]=t0+48;


	FILE* fp;
	fp=fopen(filename,"wb");

	int i1=0;
	while(i1!=MAX_BUFFER)
		fputc(buffer[i1++],fp);
	fclose(fp);
}

void File_editor::IntoBuffer(char* filename)
{
	FILE* fp;
	fp=fopen(filename,"rb");

	int i1=0;
	while(!feof(fp)&&(i1<=MAX_BUFFER-1))
		buffer[i1++]=fgetc(fp);
	buffer_length=i1-1;
	fclose(fp);
}

void File_editor::OutBuffer(char* filename)
{
	FILE* fp;
	fp=fopen(filename,"wb");

	int i1=0;
	while(i1!=MAX_BUFFER)
		fputc(buffer[i1++],fp);
	fclose(fp);
}

