#include"../SS.h"

Data_buffer::Data_buffer(int num)
{
	MAX_BUFFER=num;
	buffer=(unsigned char*)malloc(MAX_BUFFER*sizeof(unsigned char));
	int i1;
    for(i1=0;i1<=MAX_BUFFER-1;i1++)
	buffer[i1]=0x0;
	buffer_length=0;
}

Data_buffer::~Data_buffer()
{
	free(buffer);
}

void Data_buffer::ClearBuffer()
{int i1;
for(i1=0;i1<=MAX_BUFFER-1;i1++)
	buffer[i1]=0;
buffer_length=0;
}

void Data_buffer::Resize(int num)
{
	unsigned char *p=(unsigned char *)malloc(num*sizeof(unsigned char));
	int i1;
	for(i1=0;i1<=num-1;i1++)
	{
		if(i1<=MAX_BUFFER-1)
			p[i1]=buffer[i1];
		else
			p[i1]=0;
	}

	free(buffer);
	buffer=p;
	MAX_BUFFER=num;
}

