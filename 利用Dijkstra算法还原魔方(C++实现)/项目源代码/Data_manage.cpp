#include"SS.h"

bool Data_manage::save_instruct(Instruct_table ins,bool* is_storable,int lengOfstorable)
{
	ClearBuffer();
	present_ptr=0;
	unsigned short i1;
	for(i1=0;i1<=lengOfstorable-1;i1++)
	{
		if(is_storable[i1])
		{
			ushortTouchar(i1,buffer+present_ptr,buffer+present_ptr+1);
			ushortTouchar((unsigned short)(ins.ins[i1].length),buffer+present_ptr+2,buffer+present_ptr+3);
			int i2;
			for(i2=0;i2<=19;i2++)
			{
				unsigned short t=ins.ins[i1].content[i2];
				ushortTouchar(t,buffer+present_ptr+4+2*i2,buffer+present_ptr+5+2*i2);
			}
			present_ptr+=44;
		}
	}
	OutBuffer(main_instruct);
	return 1;
}

bool Data_manage::load_instruct(Instruct_table &ins)
{
	ClearBuffer();
	IntoBuffer(main_instruct);
	int num=numOfinstruct();

	unsigned short (*p)[20]=new unsigned short[num+1][20];
	int i1,i2;
	present_ptr=44;
	for(i1=1;i1<=num;i1++)
	{
        for(i2=0;i2<=19;i2++)
		   {
			   p[i1][i2]=ucharToushort(*(buffer+present_ptr+4+2*i2),*(buffer+present_ptr+5+2*i2));
		   }

		int length=ucharToushort(*(buffer+present_ptr+2),*(buffer+present_ptr+3));
        ins.ins[i1].init_ins_List(length,p[i1]);
	    present_ptr+=44;
	    continue;	
	}
	ins.ins[0].init_ins_List(0,p[0]);
	ins.next_ele=num+1;

	return 1;
}

int Data_manage::numOfinstruct()
{
	int present_ptr1=44;
	while(*(buffer+present_ptr1+2)||*(buffer+present_ptr1+3))
		present_ptr1+=44;
	return present_ptr1/44-1;
}

Data_manage::Data_manage(int num,char* mainfile,char* assistfile):File_editor(num)
{
	present_ptr=0;
	assis_instruct=assistfile;
	main_instruct=mainfile;
}

bool Data_manage::arrange_instruct()
{
	present_ptr=44;

	while(*(buffer+present_ptr)||*(buffer+present_ptr+1))
	{
		int i1;
		for(i1=0;i1<=19;i1++)
		{
			unsigned short t=ucharToushort(*(buffer+present_ptr+4+2*i1),*(buffer+present_ptr+5+2*i1));
			static int index=0;
			if(find_index(t,index))
			{
				ushortTouchar((unsigned short)index,buffer+present_ptr+4+2*i1,buffer+present_ptr+5+2*i1);
			}
		}
		present_ptr+=44;
	}
	
	for(present_ptr-=44;present_ptr>=44;present_ptr-=44)
	{
		ushortTouchar((unsigned short)(present_ptr/44),buffer+present_ptr,buffer+present_ptr+1);
	}
	return 1;
}

bool Data_manage::find_index(unsigned short behave,int &index)
{
	unsigned short present_ptr1=0;
	while(*(buffer+present_ptr1)||*(buffer+present_ptr1+1))
	{
		if((*(buffer+present_ptr1)==behave-((behave>>8)<<8))&&(*(buffer+present_ptr1+1)==behave>>8))
		{
			index=present_ptr1/44;
			return 1;
		}
		present_ptr1+=44;
	}
	return 0;
}

unsigned short Data_manage::ucharToushort(unsigned char low,unsigned char high)
{
	unsigned short t=high;
	t=(t<<8)+low;
	return t;
}

void Data_manage::ushortTouchar(unsigned short t,unsigned char *low,unsigned char *high)
{
	*low=(t<<8)>>8;
	*high=t>>8;
}

bool Data_manage::save_instruct_key(MFG &pwh)
{
	ClearBuffer();
	int i1,begin=0;
	for(i1=1;i1<=pwh.level;i1++)
	{	
    	int i3=0;
		while(pwh.Graphics[i1-1].first_status[i3].donotvisited==1)
			i3++;

		int i2=0;	
        Behave *p=pwh.Graphics[i1-1].first_status[i3].behavehand.first_behave;
		while(p)
		{
			this->ushortTouchar(p->name,buffer+begin+2*i2,buffer+begin+2*i2+1);
			i2++;
			p=p->next_behave;
		}
		begin+=40;
	}
	OutBuffer(assis_instruct);
	return 1;
}

bool Data_manage::load_instruct_key(MFG &pwh,Instruct_List ins[MAXLISH])
{
	ClearBuffer();
	IntoBuffer(assis_instruct);

	int i1,level_begin=1;
	int num=18;
	for(i1=0;i1<=num;i1++)
	{

		load_key_to_G(pwh.Graphics[i1],ins,level_begin);

		pwh.level++;
	}
	pwh.level--;
	return 1;
}

bool Data_manage::load_key_to_G(GraphicOfStatus &map,Instruct_List ins[MAXLISH],int &level)
{
	int i2=0;
	unsigned short t;
	int begin=40*(level-1);

		while(i2<=19)
	{
		if(!(*(buffer+begin+2*i2)||*(buffer+begin+2*i2+1))) {break;}
		t=ucharToushort(*(buffer+begin+2*i2),*(buffer+begin+2*i2+1));
        map.renew_Data_net(t,ins);
		i2++;
	}
	level++;
	return 1;
}

int Data_manage::numOflayer()
{
	int present_ptr1=18*40;
	while(!(*(buffer+present_ptr1+2)||*(buffer+present_ptr1+3)))
		present_ptr1-=40;
	return present_ptr1/40-2;
}

void Data_manage::save_instruct_key(behave_buffer &buffer,int level)
{
	IntoBuffer("binstruct.txt");
	unsigned char *begin=this->buffer+(level-1)*40;
	int i1;
	for(i1=0;i1<=19;i1++)
	{
		begin[2*i1]=(buffer.buffer[i1]<<8)>>8;
        begin[2*i1+1]=buffer.buffer[i1]>>8;
	}
	OutBuffer("binstruct.txt");
}



