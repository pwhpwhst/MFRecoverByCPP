#pragma once
#include<iostream>
#include<stdlib.h>

class PictAndReveal
{
public:
	void pictMF(unsigned char origin[][8], unsigned char rundant[][8]);

	void pictMF(unsigned char a[][8],char* origin_filename);

	void pictMF(unsigned char a[][8],unsigned char status[6]);

	void reveal_MF(unsigned char a[][8]);

private:
	void pictMF_fromFile(unsigned char a[][8], const char*filename);

	unsigned char col_to_num(unsigned char col);

	unsigned char anti_col_to_num(unsigned char col);

	void reveal_one_facet(unsigned char a[][8], unsigned char face);

private:
	static unsigned char pos_convey[6][9];
	static unsigned int grid_face[7];
	static unsigned char convey[20][6];
};