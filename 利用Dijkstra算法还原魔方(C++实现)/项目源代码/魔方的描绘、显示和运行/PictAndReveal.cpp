#include"../SS.h"

void PictAndReveal::pictMF(unsigned char a[][8],char* origin_filename)//以“pictureMF_0.txt”为例
{const int STRING_length=16;
char c[STRING_length]={0};
strcpy(c,origin_filename);

  while(*(c+10)!='6')//根据文件系列pictureMF_X对魔方进行涂色
     {
     pictMF_fromFile(a,c);
     c[10]=*(c+10)+1;
     }
}

void PictAndReveal::pictMF(unsigned char origin[][8], unsigned char rundant[][8])
{
	int i1,i2;
	for(i1=0;i1<=5;i1++)
		for(i2=0;i2<=7;i2++)
			rundant[i1][i2]=origin[i1][i2];
}

void PictAndReveal::pictMF(unsigned char a[][8],unsigned char status[6])
{
	unsigned char fac,pos,col;
	int grid=0;
	unsigned int t=grid_face[status[0]]&grid_face[status[2]]&grid_face[status[4]];
	while(t!=0)
	{t=t>>1;
	grid++;
	}

	int i1;
	for(i1=0;i1<=2;i1++)
	{
		fac=status[2*i1];
		if(fac!=6)
		{
		pos=convey[grid-1][2*i1+1];
		a[fac][pos-1]=status[2*i1+1];
		}
	}
}

void PictAndReveal::reveal_MF(unsigned char a[][8])
{
int i1;
    for(i1=0;i1<=5;i1++)
       {
         reveal_one_facet(a,i1);
         printf("\n");
       }
}

unsigned char PictAndReveal::anti_col_to_num(unsigned char col)
{
	switch(col)
	{
	case 0: return 'w';break;
	case 1: return 'y';break;
	case 2: return 'g';break;
	case 3: return 'b';break;
	case 4: return 'r';break;
	case 5: return 'o';break;
	}
}

void PictAndReveal::reveal_one_facet(unsigned char a[][8], unsigned char face)
{
	int i1,i2;
	unsigned char col,pos;

	for(i1=0;i1<=2;i1++)
	{
		for(i2=0;i2<=2;i2++)
		{
			if((i1*3+i2)!=4)
			{
			pos=pos_convey[face][i2+(i1*3)];
			col=a[face][pos-1];
			col=anti_col_to_num(col);
			printf("%c ",col);
			}
			else printf("%c ",anti_col_to_num(face));
		}
	printf("\n");
	}
}

unsigned char PictAndReveal::col_to_num(unsigned char col)
{
	switch(col)
	{
	case 'w': return 0;break;
	case 'y': return 1;break;
	case 'g': return 2;break;
	case 'b': return 3;break;
	case 'r': return 4;break;
	case 'o': return 5;break;
	}
}

void PictAndReveal::pictMF_fromFile(unsigned char a[][8], const char*filename)
{
	unsigned char col,colmain;
	int i1=0,pos=0,i2;

	FILE* fp=fopen(filename,"rb");//获知要处理哪个颜色的面
	fseek(fp,6,0);
	colmain=getc(fp);
	colmain=col_to_num(colmain);
	fseek(fp,0,0);

	for(i1=0,i2=0;i1<=12;i1++)
	{//涂色
	if((i1!=3)&&(i1!=4)&&(i1!=8)&&(i1!=9)&&(i1!=6))
	     {
            A:pos=pos_convey[colmain][i2++];
	          if(!pos) goto A;
	          col=getc(fp);
	          col=col_to_num(col);
	          a[colmain][pos-1]=col;
	     }
	else 
		if(i1!=12) fseek(fp,1L,1);
	}
	fclose(fp);
}

unsigned char PictAndReveal::pos_convey[6][9]={{8,1,2,7,0,3,6,5,4},
{6,5,4,7,0,3,8,1,2},{6,7,8,5,0,1,4,3,2},{6,5,4,7,0,3,8,1,2},
{8,1,2,7,0,3,6,5,4},{6,5,4,7,0,3,8,1,2}};

unsigned int PictAndReveal::grid_face[7]={255,1044480,58126,920800,539011,230968,352085};

unsigned char PictAndReveal::convey[20][6]={{0,1,6,9,4,5},{0,2,2,6,4,4},{0,3,2,5,6,9},{0,4,2,4,5,2},{0,5,6,9,5,1},
{0,6,3,8,5,8},{0,7,3,7,6,9},{0,8,3,6,4,6},{6,9,2,7,4,3},{6,9,2,3,5,3},{6,9,3,1,5,7},{6,9,3,5,4,7},
{1,5,6,9,4,1},{1,4,2,8,4,2},{1,3,2,1,6,9},{1,2,2,2,5,4},{1,1,6,9,5,5},{1,8,3,2,5,6},{1,7,3,3,6,9},{1,6,3,4,4,8}};