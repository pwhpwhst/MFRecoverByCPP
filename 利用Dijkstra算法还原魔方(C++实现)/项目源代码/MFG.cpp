#include"SS.h"
MFG::MFG(unsigned char recover_order1[20])
{
	int i1;
	level=0;
	for(i1=0;i1<=19;i1++)
		recover_order[i1]=0;

	unsigned int grid_need_compare=0,u=1;
	for(i1=0;i1<=19;i1++)
	{
		if(recover_order1[i1])
		{
			level++;
			recover_order[i1]=recover_order1[i1];
			if(i1!=0)
			grid_need_compare+=u<<recover_order1[i1-1]-1;
			Graphics[i1].Init_GraphicOfStatus(grid_need_compare,recover_order1[i1]);
		}
		else break;
	}
}

MFG::MFG(unsigned char recover_order1[20],int level1)
{
	int i1;
	level=level1;
	for(i1=0;i1<=19;i1++)
		recover_order[i1]=0;

	unsigned int grid_need_compare=0,u=1;
	for(i1=0;i1<=19;i1++)
	{
		if(recover_order1[i1])
		{
			recover_order[i1]=recover_order1[i1];
			if(i1!=0)
			grid_need_compare+=u<<recover_order1[i1-1]-1;
			Graphics[i1].Init_GraphicOfStatus(grid_need_compare,recover_order1[i1]);
		}
		else break;
	}
}

void MFG::renew_nets(unsigned char behavesFornets[20][20],Instruct_List ins[MAXLISH])
{
	int i1,i2=0;
	for(i1=1;i1<=level;i1++)
	{
		while((i2<=19)&&(behavesFornets[i1-1][i2]))
		{
            Graphics[i1-1].renew_Data_net(behavesFornets[i1-1][i2],ins);
			i2++;
		}
		i2=0;
	}
}

void MFG::renew_mark(short pmark[20][24],short mark[20][24],Instruct_List ins[MAXLISH])
{
	int i1;
	Score score_tool;
	unsigned char fac[3]={0},status[6]={0};
	for(i1=1;i1<=level;i1++)
	{
		score_tool.get_fac(recover_order[i1-1],fac);
		int i2;
		for(i2=0;i2<=5;i2++)
		{
			status[i2]=fac[(i2-i2%2)/2];
		}
		Status *k=Graphics[i1-1].find_status(status);
		Graphics[i1-1].renew_net_mark(k,pmark[i1-1],mark[i1-1],ins);
	}
}

void MFG::recover(unsigned char MF[6][8],short mark[20][24],Instruct_List ins[MAXLISH],short solve[20][20])
{int i1,i2;
	run run_tool("run1.txt");
	for(i1=1;i1<=level;i1++)
	{
       Score score_tool;
    int grid=recover_order[i1-1];
	unsigned char fac[3]={0};
	unsigned char status[6]={0};
	score_tool.get_fac(grid,fac);
	grid=score_tool.get_grid_from_col(MF,fac);
	score_tool.get_status(MF,grid,status);

	Status *k=Graphics[i1-1].find_status(status);

	Graphics[i1-1].sub_solve(status,solve[i1-1],mark[i1-1],ins);

	i2=0;
	while((solve[i1-1][i2])&&(i2<=19))
		run_tool.runMF(MF,solve[i1-1][i2++],ins);
	}
}

bool MFG::Init_higher(unsigned char layer)
{
	int i1=0;
	unsigned char greed_need_compare=0,u=1;
	while((i1<level)&&(recover_order[i1]!=layer))
	{
		greed_need_compare+=u<<(recover_order[i1]-1);
		i1++;
	}

	if(i1!=level)
		return 0;

	recover_order[level]=layer;
	this->Graphics[level].Init_GraphicOfStatus(greed_need_compare,layer);
	return 1;
}

void MFG::get_random(int num,short rans[10])
{
	srand((unsigned)time(NULL ));
	int i1;
	for(i1=0;i1<=num-1;i1++)
		rans[i1]=rand()%12+1;
	for(i1=num;i1<=10;i1++)
		rans[i1]=0;
}

void MFG::attach_behaves(short rans[10],short solve[20][20],unsigned short new_solve[20],int deep)
{
	int i1,i2;
	for(i1=0;i1<=19;i1++)
		new_solve[i1]=0;

	i1=0;
	while(rans[i1])
	{
		new_solve[i1]=rans[i1];
		i1++;
	}

	i2=i1;
	int level=1;
	while(level<=deep)
	{
		{
			i1=0;
			while(solve[level-1][i1])
				new_solve[i2++]=solve[level-1][i1++];
		}
		level++;
	}

}

bool MFG::acquire_new_behave(short mark[20][24],Instruct_List ins[MAXLISH],unsigned short new_solve[20])
{	
	int t=1,fail=0,numOfhavedone=0;
	short rans[20]={0};
	unsigned char MF[6][8],reserve_MF[6][8];
	int i1,i2;
	for(i1=0;i1<=5;i1++)
		for(i2=0;i2<=7;i2++)
			MF[i1][i2]=i1;

	PictAndReveal pict;
	pict.pictMF(MF,reserve_MF);
	run run_tool("run1.txt");
  

A:	if((numOfhavedone>=30)&&(fail/numOfhavedone>=0.8))
	{
		t++;
		fail=0;
		numOfhavedone=0;
	}
	if(t==5)return 0;

	get_random(t,rans);
	if(!try_creat_behaves(mark,t,rans,ins,new_solve))
		return 0;

	Behave_effect pwh;

	pwh.check_effect(0,new_solve,ins);
	unsigned int grid_need_compare=0,u=1;
	for(i1=0;i1<=level-1;i1++)
		grid_need_compare+=u<<(recover_order[i1]-1);

	if(pwh.Is_useful_behave(grid_need_compare))
		return 1;
	else
	{
      fail++;
	  numOfhavedone++;
	  pict.pictMF(reserve_MF,MF);
	  goto A;
	}
		
}

bool MFG::try_creat_behaves(short mark[20][24],int behave_length,short *rans,Instruct_List ins[MAXLISH],unsigned short new_solve[20])
{
	unsigned char MF[6][8],reserve_MF[6][8];
	int i1,i2;
	for(i1=0;i1<=5;i1++)
		for(i2=0;i2<=7;i2++)
			MF[i1][i2]=i1;

	PictAndReveal pict;
	pict.pictMF(MF,reserve_MF);
	run run_tool("run1.txt");

	for(i1=0;i1<=behave_length-1;i1++)
	run_tool.runMF(MF,rans[i1],ins);

	short solve[24][20];

	recover(MF,mark,ins,solve);

	int t=0;
	for(i1=0;i1<=level-1;i1++)
	{   i2=0;
		while(solve[i1][i2])
		{
			t++;
			i2++;
		}
	}
	if(t>=15)
		return 0;

	attach_behaves(rans,solve,new_solve,level);
	return 1;
}

void MFG::creat_higher_net(short mark[20][24],short pmark[20][24],Instruct_table &ins,Data_manage &h)
{
    if(Is_next_final_layer())
 		{level++;
		return;}
	behave_buffer buffer;
		bool is_usful=0;
	is_usful+=pre_creat_higher_net(ins);//binstruct.txt一定要正确的。

	printf("%d\n",Graphics[level].Is_Connected_Graphic());

	while(Graphics[level].Is_Connected_Graphic()!=16777215)
	{
		if((level==18)&&(is_usful==1))
			break;
		unsigned short new_solve[20],anti_new_solve[20];
		
		while(!acquire_new_behave(mark,ins.ins,new_solve));
	
		int exist_instruct;
		if(ins.Is_behaves_exist(new_solve))
			continue;
		
		exist_instruct=ins.Is_behaves_exist(new_solve,buffer.buffer);
		short length=ins.comput_behave_length(new_solve);

		if(!exist_instruct)
		{
			ins.add_ins(new_solve,length);
			get_anti_behave(new_solve,anti_new_solve);
			ins.add_ins(anti_new_solve,length);
			Graphics[level].renew_Data_net(ins.next_ele-2,ins.ins);
			Graphics[level].renew_Data_net(ins.next_ele-1,ins.ins);
			buffer.add(ins.next_ele-2);buffer.add(ins.next_ele-1);
			printf("%d\n",Graphics[level].Is_Connected_Graphic());	
			if(level==18)
				is_usful++;
		}
	}
	h.save_instruct_key(buffer,level+1);
	Graphics[level].renew_net_mark(pmark[level],mark[level],ins.ins);
	level++;
}

void MFG::pre_creat_higher_net(short mark[20][24],Instruct_table &ins)
{
	int i1;
	Behave_effect k;

	unsigned int grid_need_compare=0,u=1;
	for(i1=1;i1<=level;i1++)
		grid_need_compare+=u<<(recover_order[i1-1]-1);

	for(i1=1;i1<=ins.next_ele-1;i1++)
	{
		if(k.Is_useful_behave(i1,ins.ins,grid_need_compare))
			Graphics[level].renew_Data_net(i1,ins.ins);
	}
}

bool MFG::Is_next_final_layer()
{
	unsigned int grid_need_compare=0,u=1;
	int i1;
	for(i1=0;i1<=level;i1++)
	{
		grid_need_compare+=u<<(recover_order[i1]-1);
	}

	unsigned char t=recover_order[level];
	if((t==1)||(t==3)||(t==5)||(t==7)||(t==9)||(t==10)||(t==11)||(t==12)||(t==13)||(t==15)||(t==17)||(t==19))
	{
		unsigned int u1=0x55F55;
		if((u1&grid_need_compare)==u1)
			return 1;
		return 0;
	}
	else
	{
		unsigned int u1=0xAA0AA;
		if((u1&grid_need_compare)==u1)
			return 1;
		return 0;
	}
}

void MFG::Init_bottom_net(Instruct_table &ins)
{
	int i1;
	for(i1=1;i1<=12;i1++)
	{
	    Graphics[0].renew_Data_net(i1,ins.ins);
	}
}

bool MFG::ck_useful_behave(short mark[20][24],Instruct_table &ins,bool k[MAXLISH])
{
	int i1;
	bool buffer_k[MAXLISH];

	for(i1=1;i1<=level;i1++)
	{
		int i2=0;
		for(i2=0;i2<=MAXLISH-1;i2++)
			buffer_k[i2]=0;
		
		if(Graphics[i1-1].ck_useful_behave(mark[i1-1],ins,buffer_k))
		{
			for(i2=0;i2<=MAXLISH-1;i2++)
			k[i2]|=buffer_k[i2];
		}
	}
	return 1;
}

bool MFG::pre_creat_higher_net(Instruct_table &ins)
{int i1;

unsigned int grid_need_compare=0,u=1;
for(i1=1;i1<=level;i1++)
	grid_need_compare+=u<<(recover_order[i1-1]-1);

 bool Is_useful=0;

for(i1=1;i1<=level;i1++)
{
     behave_buffer buffer;
	 buffer.load_instructs("binstruct.txt",i1);
	 int i2=0;
	
	 while(buffer.buffer[i2])
	 {    
		 Behave_effect k;
		if(k.Is_useful_behave(buffer.buffer[i2],ins.ins,grid_need_compare))
		{
			Behave *p=Graphics[level].first_status[23].behavehand.first_behave;
			while(p)
			{
				if(p->name==buffer.buffer[i2])
					break;
				p=p->next_behave;
			}
			if(p==NULL)
			{
              Graphics[level].renew_Data_net(buffer.buffer[i2],ins.ins);
			  Is_useful=1;
		    }
			}
			
		 i2++;
	 }
}
return Is_useful;

}


