
class Data_manage:public File_editor
{
public:
	Data_manage(int num,char* mainfile,char* assistfile);
	bool save_instruct(Instruct_table ins,bool* is_storable,int lengOfstorable);
	bool load_instruct(Instruct_table &ins);
	bool save_instruct_key(MFG &pwh);
	bool load_instruct_key(MFG &pwh,Instruct_List ins[MAXLISH]);
	bool load_key_to_G(GraphicOfStatus &map,Instruct_List ins[MAXLISH],int &level);
	void save_instruct_key(behave_buffer &buffer,int level);
private:
	bool arrange_instruct();
	bool renew_behaves(unsigned short pre_behave,unsigned now_behave);
	bool find_index(unsigned short behave,int &index);
	int numOfinstruct();
	int numOflayer();
	void ushortTouchar(unsigned short t,unsigned char *low,unsigned char *high);
	unsigned short ucharToushort(unsigned char low,unsigned char high);
	char* main_instruct;
	char *assis_instruct;
	int present_ptr;
};

