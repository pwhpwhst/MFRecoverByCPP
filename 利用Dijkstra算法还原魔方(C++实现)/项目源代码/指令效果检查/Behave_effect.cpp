#include"../SS.h"

Behave_effect::Behave_effect()
{
	behave=0;
	oriMF=new unsigned char[6][8];
	afterMF=new unsigned char[6][8];

	PictAndReveal pict;
	Is_anti=0;
	pict.pictMF(oriMF,"pictureMF_0.txt");
	pict.pictMF(oriMF,afterMF);
}

void Behave_effect::check_effect(bool Is_anti1,unsigned short behave1,Instruct_List inc_list[MAXLISH])
{
	if((Is_anti!=Is_anti1)||(behave!=behave1))
	{
	Is_anti=Is_anti1;
	behave=behave1;
	PictAndReveal pict;
	pict.pictMF(oriMF,afterMF);


	run run_tool("run1.txt");
	if(Is_anti==0)
	run_tool.runMF(afterMF,behave1,inc_list);
	else
	run_tool.Anti_runMF(afterMF,behave1,inc_list);
	}
}

void Behave_effect::check_effect(bool Is_anti1,unsigned short* behave1,Instruct_List inc_list[MAXLISH])
{    
	Is_anti=Is_anti1;
	behave=65535;
	PictAndReveal pict;
	pict.pictMF(oriMF,afterMF);
	int i1;

	run run_tool("run1.txt");
	if(Is_anti==0)
	{
		i1=0;
		while(behave1[i1]&&(i1<=19))
		run_tool.runMF(afterMF,behave1[i1++],inc_list);
	}
	else
	{
		i1=0;
		while(behave1[i1]&&(i1<=19))
		run_tool.Anti_runMF(afterMF,behave1[i1++],inc_list);
	}
}

void Behave_effect::double_status_change(unsigned char astatus[6],unsigned char bstatus[6],unsigned char col[3])
{
	unsigned char k[6]={0};
	int i1,i2,i3=0;
	for(i1=1;i1<=5;i1=i1+2)
		for(i2=1;i2<=5;i2=i2+2)
		{
			if(astatus[i1]==bstatus[i2])
			{
				k[i3++]=i1;k[i3++]=i2;
			}
		}

		i1=0;i2=0;
while(i1<=4)
{
	if(astatus[k[i1]]!=6)
    {
	astatus[k[i1]]=bstatus[k[i1+1]]=col[i2];
	i2++;
	}
	i1+=2;
}
}

void Behave_effect::newAndoldstatus(unsigned char astatus[6],unsigned char bstatus[6])
{
	Score score_tool;
	int agrid=score_tool.get_grid_from_status_fac(astatus);
	unsigned char acol[3]={0};
	score_tool.get_cols(oriMF,agrid,acol);
	int bgrid=score_tool.get_grid_from_col(afterMF,acol);

	unsigned char arstatus[6]={0};
	score_tool.get_status(oriMF,agrid,arstatus);
	score_tool.get_status(afterMF,bgrid,bstatus);

	int i1;
	for(i1=0;i1<=2;i1++)
	{
	acol[i1]=astatus[2*i1+1];
	}
	arrange_cols(acol);


	double_status_change(arstatus,bstatus,acol);
}

void Behave_effect::check_effect_to_status(bool Is_anti1,unsigned short behave1,Instruct_List inc_list[MAXLISH],
		unsigned char astutus[6],unsigned char bstutus[6])
{
	check_effect(Is_anti1,behave1,inc_list);
	newAndoldstatus(astutus,bstutus);
}

void Behave_effect::check_effect_to_status(unsigned char astutus[6],unsigned char bstutus[6])
{
	newAndoldstatus(astutus,bstutus);
}
	
unsigned int Behave_effect::check_effect_to_grids(bool Is_anti1,unsigned short behave1,Instruct_List inc_list[MAXLISH],
		unsigned int grid_need_compare)
{
	check_effect(Is_anti1,behave1,inc_list);
	return check_effect_to_grids(grid_need_compare);
	
}

unsigned int Behave_effect::check_effect_to_grids(unsigned int grid_need_compare)
{
	int i1;
	unsigned int t=0,u=1;
	Score score_tool;
	unsigned char pre_status[6]={0},now_status[6]={0};
	for(i1=1;i1<=20;i1++)
	{
		score_tool.get_status(oriMF,i1,pre_status);
		score_tool.get_status(afterMF,i1,now_status);
		if(!score_tool.Is_the_same_status(pre_status,now_status))
		{
			t+=u<<(i1-1);
		}
	}

	return (grid_need_compare&(~t));
}

bool Behave_effect::Is_useful_behave(unsigned short behave1,Instruct_List inc_list[MAXLISH],unsigned int grid_need_compare)
{
	unsigned int t=check_effect_to_grids(0,behave1,inc_list,grid_need_compare);
	if(t!=grid_need_compare) return 0;
	Score score_tool;
	if(score_tool.Is_the_same_status(oriMF,afterMF,1048575))
		return 0;
	return 1;
}

bool Behave_effect::Is_useful_behave(unsigned int grid_need_compare)
{
	unsigned int t=check_effect_to_grids(grid_need_compare);
	if(t!=grid_need_compare) return 0;
	Score score_tool;
	if(score_tool.Is_the_same_status(oriMF,afterMF,1048575))
		return 0;
	return 1;
}

void arrange_cols(unsigned char col[3])
{
	int i1;
	for(i1=0;i1<=1;i1++)
		if(col[i1]==6)
	break;
	switch(i1)
	{
	case 0:col[0]=col[1];
	case 1:col[1]=col[2];col[2]=6;break;
	}

}
