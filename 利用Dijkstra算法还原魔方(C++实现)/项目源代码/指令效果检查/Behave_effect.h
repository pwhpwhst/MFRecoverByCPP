
class Behave_effect
{
	friend void main();
	friend void arrange_cols(unsigned char col[3]);
public:
	unsigned short behave;
	bool Is_anti;
	unsigned char (*oriMF)[8];
	unsigned char (*afterMF)[8];
public:
	Behave_effect();
	void check_effect(bool Is_anti1,unsigned short behave1,Instruct_List inc_list[MAXLISH]);
	void check_effect_to_status(bool Is_anti1,unsigned short behave1,Instruct_List inc_list[MAXLISH],
		unsigned char astutus[6],unsigned char bstutus[6]);
	unsigned int check_effect_to_grids(bool Is_anti1,unsigned short behave1,Instruct_List inc_list[MAXLISH],
		unsigned int grid_need_compare);
	bool Is_useful_behave(unsigned short behave1,Instruct_List inc_list[MAXLISH],unsigned int grid_need_compare);
	void check_effect(bool Is_anti1,unsigned short* behave1,Instruct_List inc_list[MAXLISH]);
	void check_effect_to_status(unsigned char astutus[6],unsigned char bstutus[6]);
	unsigned int check_effect_to_grids(unsigned int grid_need_compare);
	bool Is_useful_behave(unsigned int grid_need_compare);
private:
	void double_status_change(unsigned char astatus[6],unsigned char bstatus[6],unsigned char col[3]);
	void newAndoldstatus(unsigned char astatus[6],unsigned char bstatus[6]);
};



