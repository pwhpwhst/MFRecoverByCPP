#include"../SS.h"



unsigned char Score::get_col(unsigned char a[6][8],unsigned char fac,unsigned  pos)
{
if(fac==6) return 6;
return a[fac][pos-1];
}

void Score::get_status(unsigned char MF[6][8],int grid,unsigned char *status)
{int i1;
	unsigned char fac,pos;
	for(i1=0;i1<=2;i1++)
	{
		fac=convey[grid-1][2*i1];
		pos=convey[grid-1][2*i1+1];
		status[2*i1]=fac;
		if(fac!=6)
		status[2*i1+1]=MF[fac][pos-1];
		else
		status[2*i1+1]=6;
	}
}

int Score::get_grid_from_status_col(unsigned char MF[6][8],unsigned char *status)
{
	int i1;

	unsigned char try_col[3]={0},col[3]={0};
	for(i1=0;i1<=2;i1++)
		col[i1]=status[2*i1];

	for(i1=1;i1<=20;i1++)
	{
		get_cols(MF,i1,try_col);

		if(Is_the_same_cols(col,try_col)) return i1;
	}
	return 0;
}

int Score::which_grid(unsigned char fac[3])
{
	const unsigned int k[7]={0x000000FF,0x000FF000,0x0000E30E,0x000E0CE0,0x00083983,0x00038638,0x00055F55};
	unsigned int t=k[fac[0]]&k[fac[1]]&k[fac[2]];
	int i1=0;
	while(t)
	{
		t=t>>1;
		i1++;
	}
	return i1;
}

int Score::get_grid_from_status_fac(unsigned char *status)
{
	int i1;
	unsigned char fac[3]={0};
	for(i1=0;i1<=2;i1++)
	{
		fac[i1]=status[2*i1];
	}
	
	return which_grid(fac);
}

int Score::Is_the_same_status(unsigned char a[6][8],unsigned char b[6][8],unsigned int grid_need_compare)
{
	int i1,i2;
	unsigned char a_col,b_col;

	for(i1=1;i1<=20;i1++)
	{
		if((grid_need_compare<<(32-i1))>>31)
		for(i2=0;i2<=5;i2=i2+2)
		{
			a_col=get_col(a,convey[i1-1][i2],convey[i1-1][i2+1]);
			b_col=get_col(b,convey[i1-1][i2],convey[i1-1][i2+1]);
			if(a_col!=b_col) return 0;
		}
	}
	return 1;
}

int Score::Is_the_same_status(unsigned char* status1,unsigned char* status2)
{int i1;
   for(i1=0;i1<=5;i1++)
   {
	   if(status1[i1]!=status2[i1]) return 0;
   }
   return 1;
}

int Score::Is_the_same_cols(unsigned char col1[3],unsigned char col2[3])
{int i1,i2;
   for(i1=0;i1<=2;i1++)
   {
   for(i2=0;i2<=2;i2++)
   {
	   if(col1[i1]==col2[i2]) break;
   }
   if(i2==3)
   return 0;
   }
   return 1;
}

int Score::Is_perfect_status(unsigned char *status)
{
	int i1=0;
	for(i1=0;i1<=2;i1++)
		if(status[2*i1]!=status[2*i1+1]) return 0;
	return 1;
}

void Score::get_fac(int grid,unsigned char fac[3])
{
	fac[0]=convey[grid-1][0];
	fac[1]=convey[grid-1][2];
	fac[2]=convey[grid-1][4];
}



void Score::get_cols(unsigned char MF[6][8],int grid,unsigned char col[3])
{int i1;
	unsigned char fac,pos;
	for(i1=0;i1<=2;i1++)
	{
		fac=convey[grid-1][2*i1];
		pos=convey[grid-1][2*i1+1];
		if(fac!=6)
		col[i1]=MF[fac][pos-1];
		else
		col[i1]=6;
	}
}

int Score::get_grid_from_col(unsigned char MF[6][8],unsigned char col[3])
{
	int i1;
	unsigned char try_col[3]={0};
	for(i1=1;i1<=20;i1++)
	{
		get_cols(MF,i1,try_col);
		if(Is_the_same_cols(col,try_col)) return i1;
	}
	return 0;
}

unsigned int Score::grids_not_completed(unsigned char MF[6][8],unsigned int grid_need_compare)
{
int i1;
unsigned int t=0,t1=1;
unsigned char status[6]={0};
for(i1=1;i1<=20;i1++)
  {
	if(grid_need_compare%2==1)
	{
		get_status(MF,i1,status);
		if(!Is_perfect_status(status))
		{
			t+=t1;
		}
	}
	grid_need_compare=grid_need_compare>>1;
	t1=t1<<1;
   }
return t;
}

unsigned char Score::convey[20][6]={{0,1,6,9,4,5},{0,2,2,6,4,4},{0,3,2,5,6,9},{0,4,2,4,5,2},{0,5,6,9,5,1},
{0,6,3,8,5,8},{0,7,3,7,6,9},{0,8,3,6,4,6},{6,9,2,7,4,3},{6,9,2,3,5,3},{6,9,3,1,5,7},{6,9,3,5,4,7},
{1,5,6,9,4,1},{1,4,2,8,4,2},{1,3,2,1,6,9},{1,2,2,2,5,4},{1,1,6,9,5,5},{1,8,3,2,5,6},{1,7,3,3,6,9},{1,6,3,4,4,8}};

//unsigned char Score::get_grid[6][8]={{1,2,3,4,5,6,7,8},{17,16,15,14,13,20,19,18},{15,16,10,4,3,2,9,14},
//{11,18,19,20,12,8,7,6},{13,14,9,2,1,8,12,20},{5,4,10,16,17,18,11,6}};

