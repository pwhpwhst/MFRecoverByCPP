#pragma once

class Score
{
public:

	int Is_the_same_cols(unsigned char col1[3],unsigned char col2[3]);

	int Is_the_same_status(unsigned char a[6][8],unsigned char b[6][8],unsigned int grid_need_compare);

    int Is_the_same_status(unsigned char* status1,unsigned char* status2);

	int Is_perfect_status(unsigned char *status);

	int which_grid(unsigned char fac[3]);

    int get_grid_from_status_col(unsigned char MF[6][8],unsigned char *status);

    int get_grid_from_col(unsigned char MF[6][8],unsigned char col[3]);

	unsigned char get_col(unsigned char a[6][8],unsigned char fac,unsigned  pos);

	void get_status(unsigned char MF[6][8],int grid,unsigned char* status);

	void get_cols(unsigned char MF[6][8],int grid,unsigned char col[3]);

	void get_fac(int grid,unsigned char fac[3]);

	unsigned int grids_not_completed(unsigned char MF[6][8],unsigned int grid_need_compare);

	int get_grid_from_status_fac(unsigned char *status);

private:
	static unsigned char convey[20][6];

//	static unsigned char get_grid[6][8];
	 
};
