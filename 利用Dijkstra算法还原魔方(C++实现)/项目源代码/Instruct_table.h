class Behave;
class HandOfbehave;
class MFG;
class Instruct_table
{
public:
	int next_ele;
	const static int MAX=MAXLISH;
	Instruct_List *ins;
public:
	Instruct_table();
	bool add_ins(unsigned short *new_behave,int length);
	bool Is_behaves_exist(unsigned short *new_behave);
	int Is_behaves_exist(unsigned short *new_behave,unsigned short pre_behaves[20]);
    short comput_behave_length(unsigned short *new_behave);
	bool renew_behave(unsigned short *new_behave,int i1);
	bool Has_relation(unsigned short behave1,unsigned short behave2);	
	bool Has_relation(MFG &pwh,unsigned short behave1,int its_layer,bool k[MAXLISH],int lengthOfk);
	bool ck_avail_behave(MFG &pwh,bool k[MAXLISH],int lengthOfk);
	bool merge(Instruct_table ins2,unsigned short pre_behaves[20]);
	bool Is_behaves_exist(unsigned short *new_behave,int level);
private:
	void sub_Is_behaves_exist(unsigned short *new_behave,unsigned char MF_0[6][8],
		unsigned char MF_1[6][8],unsigned char MF_2[6][8]);
};